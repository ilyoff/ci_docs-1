# Руководство по настройке CI для мобильных приложений

#### Содержание
1. [Общие приготовления](#common)
2. [Настройка для IOS](#ios)
3. [Настройка для Android](#android)
4. [Настройка bitrise.io](#bitrise)
5. [Шаблон](#template)
6. [Полезные ссылки](#links)


## <a name="common"></a>Общие приготовления
Для работы нам понадобятся устрановленные XCode, Ruby, Fastlane, а также доступ к порталу разработчиков [developer.apple.com](https://developer.apple.com)

### Fastlane
[Fastlane](https://fastlane.tools/) - это набор утилит, написанных на руби, облегчающих сборку и публикацию мобильных приложений на IOS и Android.

##### Установка

**brew (Mac)**
```
brew cask install fastlane
```

**Вручную (Mac/Linux)**

Скачайте [архив](https://download.fastlane.tools/fastlane.zip) и запустите скрипт `install`.

**Через gem-ы (> Ruby 2.0) (Mac/Linux)**
```
sudo gem install fastlane -NV
```

##### Инициализация
Перейдите в терминале в корень проекта и выполните команду:
```
fastlane init
```

Ответив на несколько вопросов, в корне проекта создастся папка `fastlane`, где будут необходимые файлы конфигурации. 
Чтобы сэкономить время на настройке, можно и нужно использовать наш [шаблон]().



## <a name="ios"></a>Настройка под IOS
### Подпись приложения
Для безопасности приложений Apple использует цифровую подпись кода сертификатом. Сертификат можно создать на [портале для разработчиков](https://developer.apple.com/account/ios/certificate/) вручную. Но мы это будем делать автоматически, с помощью `fastlane`-утилиты [match](https://github.com/fastlane/fastlane/tree/master/match#readme). 
Для начала нам потребуется создать приватный репозиторий для хранения сертификатов. Для этого в корне проекта выполняем
```
match init
```

и вводим необходимую информацию, указываем и запоминаем пароль шифрования, он нам еще пригодится. В папке `fastlane` создастся конфигурационный файл `Matchfile` с необходимыми данными.
Сгенерируем необходимые сертификаты, выполнив команды:

```
match development --git-branch=<app_name>
match appstore --git-branch=<app_name>
```

Создадуться соответствующие сертификаты и сохранятся в указанной ветку в репозитории. Указание ветки позволит нам хранить сертификаты для разных приложений в одном репозитории.

### XCode
Для сборки под IOS важно настроить правильно XCode. Как это сделать, можно узнать [здесь](https://docs.fastlane.tools/codesigning/xcode-project/). Возможные проблемы описаны [здесь](https://docs.fastlane.tools/codesigning/troubleshooting/). Не помогло? Гуглите!

### Конфигурируем fastlane
В папке `fastlane` создается основной файл конфигурации - `Fastfile`. В нем создадим воркфлоу (lanes) для сборки и деплоя нашего приложения.

```
platform :ios do

    desc "Build app"
    lane :build do
        
        # Отключаем автоматическую подпись в XCode
        update_project_codesigning(
            path: './ios/myappname.xcodeproj',
            use_automatic_signing: false
        )
        
        # Скачиваем и устанавливаем сертификаты из репозитория
        match(
            type: "appstore",
            app_identifier: 'com.myappname.myappname',
            readonly: true
        )
        
        # Увеличиваем номер билда
        increment_build_number(
            xcodeproj: './ios/BizUp.xcodeproj'
        )
        
        # Собираем приложение
        @ipa = gym(
            scheme: "MyApp",
            project: "./ios/myapp.xcodeproj"
        )

    end

    desc "Submit a new Beta Build to Apple TestFlight"
    desc "This will also make sure the profile is up to date"
    lane :beta do
        # Запускаем предыдущий воркфлоу для сборки приложения
        build()
        
        # Отправляем приложение в Testflight для бета-тестирования
        pilot(
            ipa: @ipa,
            skip_waiting_for_build_processing: true,
            skip_submission: true
        )
        
        # Очищаем папку от созданных промежуточных файлов
        clean_build_artifacts
        
        # Коммитим увеличенную версию
        commit_version_bump(
            message: "Deployed new build #{lane_context[SharedValues::BUILD_NUMBER]} [skip ci]",
            xcodeproj: './ios/myappname.xcodeproj',
            force: true
        )

    end
```

Более подробную информацию по всем указанным коммандам и их опциям можно найти в [официальной документации fastlane](https://docs.fastlane.tools/).

Протестируем созданные нами воркфлоу локально:

```
fastlane build
fastlane beta
```

Если оба воркфлоу завершаются успешно, то можно переходить к настройке CI на сервере. 
## <a name="android"></a>Настройка для Android
*comming soon*

## <a name="bitrise"></a>Настройка Bitrise.io
Fastlane работает практически с любым сервером CI или сервисом. Мы будем использовать сервис [bitrise.io](https://www.bitrise.io) из-за его простоты и легкости настройки. Для этого нам понадобится, естественно, действующий аккаунт. 

1. Создаем новое приложение с соответсвующей конфигурацией (Android/IOS)
2. Переходим в раздел Workflows -> Secret Env Vars и создаем следующие переменные окружения:
    - `FASTLANE_USER` - Apple ID для доступа в Developer Portal/ITunes Connect
    - `FASTLANE_PASSWORD` - Пароль от Apple ID, указанного выше
    - `MATCH_PASSWORD` - пароль, использованный для шифрования сертификатов и профилей утилитой `match`.
    - `BITBUCKET_USER` - пользователь для доступа к репозиторию приложения
    - `BITBUCKET_PASS` - пароль пользователя
3. Создаем новый или редактируем существующий воркфлоу, переименновываем его в `build`/`deploy` и выставляем следующие "шаги" (steps):
    - Activate SSH key (RSA private key) 
    - Git Clone Repository
    - Install React Native 
    - Run npm command с командой `install`
    - Certificate and profile installer. Этот шаг в нашем случае бесполезен, но bitrise [рекомендует](http://devcenter.bitrise.io/fastlane/fastlane-tools-integration/) все равно его включать в воркфлоу
    - Run CocoaPods install
    - Script с содержанием `gem install bundler`, для обновления bundler-а
    - Fastlane. В поле `fastlane lane` указываем нужный нам воркфлоу (`build`/`beta`), который будет запускаться.
    - Script с содержанием `git push https://$BITBUCKET_USER:$BITBUCKET_PASS@bitbucket.org/smartbics/bizup.git HEAD:$BITRISE_GIT_BRANCH`. Он будет пушить увеличенную версию билда в репозиторий нашего приложения.
    
Это минимум минимальный сценарий необходимый нам для запуска наших воркфлоу.

## <a name="template"></a>Шаблон
Для ускорения настройки fastlane и bitrise.io на основе полученного опыта был создан [шаблон](template/). 
**Преимущества:**
- Прописаны все необходимые воркфлоу
- Возможность конфигурирования с помощью единого `config`-файла, так и через переменные окружения
- Интеграция с Телеграмм (пока только отправка логов в чат)

### Установка и использование
Скопируйте папку `fastlane` и файлы `Gemfile`, `Gemfile.lock` в корень проекта. Установите все зависимости.
```
bundle install
```

Сконфигурируйте файл настроек `./fastlane/config.rb`. Проверьте работоспособность сборки. 
##### Интеграция с Telegram
Создайте своего Telegram-бота и укажите его id в качестве значения переменной `COMMON_TELEGRAM_BOT`  или добавьте [@BizupDevBot](https://telegram.me/BizupDevBot) в свой чат.
Также добавьте бота [@get_id](https://telegram.me/get_id_bot) и вызовите в чате команду `/get_id`. Получив id чата, также добавьте его в конфиг.

##### Bitrise.io
Скопируйте содержимое файла `./template/bitrise.ios.yml` в раздел `Workflow -> bitrise.yml` на сайте [bitrise.io](https://www.bitrise.io), полностью заменив содержимое раздела и сохраните изменения.


**Done!!!**

## <a name="links"></a> Полезные ссылки
- [Continuous Integration for React Native Apps With Fastlane and Bitrise (iOS)](http://blog.thebakery.io/continuous-integration-for-react-native-applications-with-fastlane-and-bitrise-ios-version/)
- [Codesigning Guide](https://codesigning.guide/)
- [Fastlane official site](https://fastlane.tools/)
- [Bitrise Dev Center](http://devcenter.bitrise.io/)
- [How to automate Android build process on Bitrise CI (part 1)](https://medium.com/@hesam.kamalan/how-to-automate-android-build-process-on-bitrise-ci-71ae3a94362e#.apxtntofx)
- [How to automate Android build process on Bitrise CI (part 2)](https://medium.com/@hesam.kamalan/how-to-automate-android-build-process-on-bitrise-ci-part-2-b3f8124c29ee#.rtlt0qn3o)







