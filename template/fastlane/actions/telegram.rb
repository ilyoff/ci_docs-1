# source: https://github.com/rambler-ios/fastlane-flows/blob/master/fastlane/actions/telegram.rb
module Fastlane
  module Actions
    class TelegramAction < Action
      def self.run(params)
        text = params[:text]
        uri = URI('#{COMMON_TELEGRAM_BOT}/sendMessage')
        params = { :text => text, :chat_id => COMMON_TELEGRAM_CHAT_ID, :parse_mode => "Markdown" }
        uri.query = URI.encode_www_form(params)
        Net::HTTP.get(uri)
      end

      #####################################################
      # @!group Documentation
      #####################################################

      def self.description
        'Sends a message to Telegram chat'
      end

      def self.available_options
        [
          FastlaneCore::ConfigItem.new(key: :text,
                                       env_name: "FL_TELEGRAM_TEXT",
                                       description: "The message text",
                                       is_string: true,
                                       default_value: "")
        ]
      end

      def self.output
        [
          ['TELEGRAM_TEXT', 'The message text']
        ]
      end

      def self.authors
        ["etolstoy"]
      end

      def self.is_supported?(platform)
        true
      end
    end
  end
end
