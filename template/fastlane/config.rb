# COMMON
COMMON_BUILD_URL = ENV['BITRISE_BUILD_URL']
COMMON_SKIP_INCREMENT = ENV['COMMON_SKIP_INCREMENT']

# BizupDevBot
COMMON_TELEGRAM_BOT = ENV['COMMON_TELEGRAM_BOT'] || 'https://api.telegram.org/bot295882223:AAFswA10fMmrBhL2QZ4cWg39hQCZviIZQk4'

# source: http://stackoverflow.com/questions/32423837/telegram-bot-how-to-get-a-group-chat-id-ruby-gem-telegram-bot
# 
# Add the Telegram BOT to the group.
# Get the list of updates for your BOT:
#
# https://api.telegram.org/bot<YourBOTToken>/getUpdates
# Ex:
# https://api.telegram.org/botjbd78sadvbdy63d37gda37bd8/getUpdates
# 
# Look for the "chat" object:
# {
#   "update_id":8393,
#   "message":{
#     "message_id":3,
#     "from":{
#       "id":7474,
#       "first_name":"AAA"
#      },
#      "chat":{
#         "id": <CHAT_ID>,
#         "title":"TITLE"
#      },
#      "date":25497,
#      "new_chat_participant":{
#        "id":71,
#        "first_name":"NAME",
#        "username":"YOUR_BOT_NAME"
#      }
# }
# 
# This is a sample of the response when you add your BOT into a group.
# 
# Use the "id" of the "chat" object to send your messages.


COMMON_TELEGRAM_CHAT = ENV['COMMON_TELEGRAM_CHAT'] || '<telegram_chat_id>'




# IOS
IOS_APP_IDENTIFIER = ENV['IOS_APP_IDENTIFIER'] || 'com.mybizup.bizUp'
IOS_PROJECT_PATH = ENV['IOS_PROJECT_PATH'] || './ios/BizUp.xcodeproj'
IOS_MATCH_USERNAME = ENV['IOS_MATCH_USERNAME'] || 'ilyoff@gmail.com'
IOS_MATCH_GIT_URL = ENV['IOS_MATCH_GIT_URL'] || 'git@bitbucket.org:smartbics/fastlane.git'
IOS_APPLE_ID = ENV['IOS_APPLE_ID'] || 'ilyoff@gmail.com'
IOS_TEAM_ID = ENV['IOS_TEAM_ID'] || "LQF8383U3Q"


